package com.example.a003257591.lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button press;

    EditText current;
    EditText affected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        press = (Button)findViewById(R.id.button);

        current = (EditText)findViewById(R.id.editText);
        affected = (EditText)findViewById(R.id.editText2);

        press.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String inputValue = current.getText().toString();
                affected.setText(inputValue);
            }


        });
    }
}
